public class Divider {
  int divide(int x, int y){
    if(y==0) throw new ArithmeticException("Div by 0 error.");
    else if(x%y!=0) throw new ArithmeticException("Can't divide "+x+" by "+y);
    return x/y;
  } 
}
