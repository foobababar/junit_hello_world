import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class DividerTest {
  @Test
  void test1(){
    Divider d = new Divider();
    assertEquals(d.divide(4,2), 2);
  }

  @Test
  void test2(){
    Divider d = new Divider();
    assertThrows(ArithmeticException.class, 
		    () -> {
                      d.divide(50, 0); 
		    });
  }

  @Test
  void test3(){
    Divider d = new Divider();
    assertEquals(d.divide(50,2), 25);
  }

  @Test
  void test4(){
    Divider d = new Divider();
    assertEquals(d.divide(0,2), 0);
  }

  @Test
  void test5(){
    Divider d = new Divider();
    assertThrows(ArithmeticException.class, 
		    () -> {
                      d.divide(13, 4); 
		    });
  }
}
